# Author: Wyatt Snyder
# Date Created: 9/19/2019
# Date Last Modified: 9/19/2019
#
# This file converts the training set into a csv for use in the training of the clustering model

import os

direc = os.getcwd()

direc = direc + '\\trainingSet'

dirs = os.listdir(direc)

print("category,filename,title,content,category_id")

for category in dirs:
    curr_dir = direc + "\\" + category
    files = os.listdir(curr_dir)
    cat_id = dirs.index(category)
    for file in files:
        with open(os.path.join(curr_dir, file))as curr_file:
            file_text = curr_file.readlines()
            title = "\"" + (("".join(file_text[0])).replace("\"", "\"\"")) + "\""
            content = "\"" + ((" ".join(file_text[2:])).replace("\"", "\"\"")) + "\""
            print(category + "," + file + "," + title + "," + content + "," + str(cat_id))

# 201903-Capstone-S01-G01

## Fall 2019.  Section 01 Group 01

###### Docker cmds inside of Backend folder
`docker build -t server:latest .`  
`docker run -p 443:5000 server`

###### Docker cmds inside of Client/client-webapp folder
`docker build -t frontend:latest .`  
`docker run -p 8080:8080 frontend`

###### Docker cmds inside of news-scraper folder
`docker build -t scraper:latest .`  
`docker run scraper`
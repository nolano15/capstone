from flask import Blueprint, jsonify, request
from flask_login import current_user, login_required
from newspaper import ArticleException
from pymongo import MongoClient
from bson import json_util
from bson.objectid import ObjectId
from login import cfg
import datetime, json, urllib.parse, newspaper
from pymongo.errors import PyMongoError

RECENT_DAYS_THRESHOLD = 2

topicPage = Blueprint("topic_page", __name__)

db = MongoClient(host="mongodb://admin:TAOD@" + cfg["DB"] + ":27017")["ambassador"]
artDB = db["article_info"]
sourceDB = db["source_list"]
userDB = db["users"]
ratingDB = db["ratings"]

@topicPage.route("/topic/", methods=['GET'])
def topics():
    return jsonify(artDB.distinct("category"))

@topicPage.route("/topic/<cat>/", methods=['GET'])
def topicCat(cat):
    return pagedTopics(cat, 1)

@topicPage.route("/topic/<cat>/<page>/", methods=['GET'])
def topicCatPagenated(cat, page):
    return pagedTopics(cat, page)

def pagedTopics(cat, page):
    LIMIT = 25
    filter = {"category": cat}
    return ({"error": "No results for category {}".format(cat)} if artDB.count_documents(filter) == 0
        else json_util.dumps(artDB.find(filter, skip=LIMIT*(int(page)-1), limit=LIMIT)))

@topicPage.route("/article/<id>/", methods=['GET'])
def article(id):
    error = {"error": "Article not found"}
    if not ObjectId.is_valid(id):
        return error
    article = artDB.find_one({"_id": ObjectId(id)})
    return error if not article else json_util.dumps(article)

@topicPage.route("/source/check/", methods=['PUT'])
@login_required
def check_source():
    source = request.json
    count = sourceDB.count_documents(source)
    if count != 0:
        return {'error': source['url'] + ' is already in the database'}

    print(source['url'])
    paper = newspaper.build(source['url'])
    paper.download()
    result = []
    for article in paper.articles[:5]:
        try:
            article.download()
            article.parse()
            result.append(article.title)
        except ArticleException:
            pass
    return json.dumps(result)

@topicPage.route("/source/add/", methods=['PUT'])
@login_required
def add_source():
    source = request.get_json(force=True)
    try:
        sourceDB.insert_one(source)
    except PyMongoError:
        return {'error': 'Could not add ' + source['url'] + ' to main list'}

    current_user.sites.append(source['url'])
    try:
        userDB.update_one({'id': current_user.id}, {'$set': current_user.__dict__})
    except PyMongoError:
        return {'error': 'Could not add ' + source['url'] + ' to user list'}
    return source['url'] + ' added successfully!'

@topicPage.route("/search/<query>", methods=['GET'])
def search(query):
    error = {"error": "Could not complete search."}
    text = urllib.parse.unquote(query)
    result = None
    try:
        result = artDB.find({"$text": {"$search": text}})
    except PyMongoError:
        return error
    return error if not result else json_util.dumps(result)


@topicPage.route("/custom_topics/", methods=['GET'])
def get_topics():
    return json_util.dumps(current_user.topics)


@topicPage.route("/delete_topic/", methods=['DELETE'])
def delete_topic():
    topic = request.get_json(force=True)

    current_user.topics.remove(topic['name'])
    try:
        userDB.update_one({'id': current_user.id}, {'$set': current_user.__dict__})
    except PyMongoError:
        return {'error': 'Could not remove ' + topic['name'] + ' from user list'}
    return topic['name'] + ' removed successfully!'

@topicPage.route("/add_topic/", methods=['PUT'])
@login_required
def add_topic():
    topic = request.get_json(force=True)

    current_user.topics.append(topic['name'])
    try:
        userDB.update_one({'id': current_user.id}, {'$set': current_user.__dict__})
    except PyMongoError:
        return {'error': 'Could not add ' + topic['name'] + ' to user list'}
    return topic['name'] + ' added successfully!'


@topicPage.route("/recent/", methods=['GET'])
def recent():
    try:
        return json_util.dumps(artDB.find({"access_date": {
            "$gt": datetime.datetime.now() - datetime.timedelta(days=RECENT_DAYS_THRESHOLD)}}))
    except PyMongoError:
        return {"error": "Unable to get recent articles"}


# Expects a JSON Object like the following
# {
#  "article_id": "5dcf0f603de4eefdbba68384",
#  "rating": "like"
# }
@topicPage.route("/rate/", methods=['PUT'])
@login_required
def rate():
    rating = request.get_json(force=True)
    rating['user_id'] = current_user.id
    try:
        ratingDB.update_one({'id': current_user.id, "article_id": rating['article_id']}, {"$set": rating}, upsert=True)
        return json_util.dumps(ratingDB.find_one({"article_id": rating['article_id'], "user_id": current_user.id}))
    except PyMongoError:
        return {"error": "Unable to rate article"}


@topicPage.route("/likes/", methods=['GET'])
@login_required
def likes():
    try:
        return json_util.dumps(
            ratingDB.find({"user_id": current_user.id, "rating": "like"})
        )
    except PyMongoError:
        return {"error": "Unable to retrieve likes"}


@topicPage.route("/dislikes/", methods=['GET'])
@login_required
def dislikes():
    try:
        return json_util.dumps(
            ratingDB.find({"user_id": current_user.id, "rating": "dislike"})
        )
    except PyMongoError:
        return {"error": "Unable to retrieve dislikes"}


@topicPage.route("/rating/<article_id>/", methods=['GET'])
def get_rating(article_id):
    try:
        like_count = ratingDB.count({"article_id": article_id, "rating": "like"})
        dislike_count = ratingDB.count({"article_id": article_id, "rating": "dislike"})
        return {"article_id": article_id, "likes": str(like_count), "dislikes": str(dislike_count)}
    except PyMongoError:
        return {"error": "Unable to retrieve ratings for article."}


@topicPage.route("/get-user-rating/<article_id>/", methods=['GET'])
@login_required
def get_user_rating(article_id):
    try:
        return json_util.dumps(ratingDB.find_one({"article_id": article_id, "user_id": current_user.id}))
    except PyMongoError:
        return {"error": "Unable to retrieve rating for article."}

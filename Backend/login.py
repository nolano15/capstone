import json, os, base64, requests

with open("config.json") as config_file:
    cfg = json.load(config_file)

from user import userPage, User
from topic import topicPage
from flask import (
    Flask,
    redirect,
    request,
    url_for,
    Blueprint)
from flask_login import (
    LoginManager,
    current_user,
    login_required,
    login_user,
    logout_user)
from flask_cors import CORS
from oauthlib.oauth2 import WebApplicationClient

# google config with our app's registration codes
GOOGLE_CLIENT_ID = '503206725996-07huqe8qpt4lbph8svfl7lnfenpoam3s.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = cfg["GOOGLE_CLIENT_SECRET"]
GOOGLE_CFG = requests.get("https://accounts.google.com/.well-known/openid-configuration").json()

# oauth client
client = WebApplicationClient(GOOGLE_CLIENT_ID)

app = Flask(__name__)
app.config.update(
    SECRET_KEY=base64.b64decode(bytes(cfg["SECRET_KEY"], "utf-8")),
    SESSION_COOKIE_SECURE=True,
    SESSION_COOKIE_SAMESITE='Lax'
)
CORS(app, supports_credentials=True)

loginPage = Blueprint("login_page", __name__)
login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.unauthorized_handler
def unauthorized():
    return {"error": "You must be logged in to access this content"}

@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

def frontendRedirect():
    return redirect("http://" + cfg["HOST"])

@loginPage.route("/auth")
def userAuth():
    return {
        "isLoggedIn": current_user.is_authenticated,
        "href": url_for(".logout" if current_user.is_authenticated else ".login", _external=True)
    }

@loginPage.route("/login/")
def login():
    return redirect(client.prepare_request_uri(
        # get the url needed to redirect the user for google login
        GOOGLE_CFG["authorization_endpoint"],
        redirect_uri=request.base_url + "callback",
        # here is what we want to ask for the user's permission to use
        scope=["openid", "email", "profile"]))

@loginPage.route("/login/callback")
def callback():
    # create request for tokens
    token_url, headers, body = client.prepare_token_request(
        # get the url to where the unique code must be sent to get back tokens
        GOOGLE_CFG["token_endpoint"],
        authorization_response=request.url,
        redirect_url=request.base_url,
        # google sends back a unique code
        code=request.args.get("code"))
    # send the token request to google
    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET))

    # parse tokens
    client.parse_request_body_response(json.dumps(token_response.json()))

    # use tokens to get user's info
    uri, headers, body = client.add_token(GOOGLE_CFG["userinfo_endpoint"])
    # send request for user's info
    userinfo_response = requests.get(uri, headers=headers, data=body).json()

    unique_id = userinfo_response["sub"]
    full_name = userinfo_response["given_name"] + " " + userinfo_response["family_name"]

    user = User(
        id=unique_id,
        name=full_name,
        email=userinfo_response["email"],
        profile_pic=userinfo_response["picture"],
        sites=[],
		topics=[] # new topics
    )

    if not User.get(unique_id):
        User.put(user)

    login_user(user)

    # redirect to home page
    return frontendRedirect()

@loginPage.route("/logout")
@login_required
def logout():
    logout_user()
    return frontendRedirect()

app.register_blueprint(loginPage, url_prefix="/api")
app.register_blueprint(userPage, url_prefix="/api/user")
app.register_blueprint(topicPage, url_prefix="/api")

if __name__ == "__main__":
    app.run(ssl_context='adhoc', host='0.0.0.0')

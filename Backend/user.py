from bson import json_util
from flask import Blueprint
from flask_login import UserMixin, login_required, current_user
from pymongo import MongoClient

userPage = Blueprint("user_page", __name__)

@userPage.route("/", methods=['GET'])
@login_required
def profile():
    return current_user.__dict__

@userPage.route("/<id>/", methods=['GET'])
def profileView(id):
    user = User.get(id)
    return {"error": "User does not exist"} if not user else user.__dict__

class User(UserMixin):
    def __init__(self, id, name, email, profile_pic, sites, topics):
        self.id = id
        self.name = name
        self.email = email
        self.profile_pic = profile_pic
        self.sites = sites
        self.topics = topics

    @staticmethod
    def get(user_id):
        user = userDB.find_one({"id": user_id})
        return None if not user else User(id=user["id"], name=user["name"], email=user["email"], profile_pic=user["profile_pic"], sites=user["sites"], topics=user["topics"])

    @staticmethod
    def put(user):
        userDB.insert_one(user.__dict__)

from login import cfg
userDB = MongoClient(host="mongodb://admin:TAOD@" + cfg["DB"] + ":27017")["ambassador"]["users"]
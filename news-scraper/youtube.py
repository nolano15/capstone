import googleapiclient.discovery, datetime
from youtube_transcript_api import YouTubeTranscriptApi
from textblob import TextBlob
from googleapiclient.errors import HttpError

politicsLabel = "politics"
sportsChannels = {"MLB", "NBA", "ESPN"}
newsChannels = {
    "ABC News": "UCBi2mrWuNuyYy4gbM6fU18Q",
    "BBC News": "UC16niRr50-MSBwiO3YDb3RA",
    "CBS News": "UC8p1vwvWtl6T73JiExfWs1g",
    "CNN": "UCupvZG-5ko_eiXAupbDfxWw",
    "The Daily Wire": "UCaeO5vkdj5xOQHp4UmIN6dw",
    "Fox News": "UCXIJgqnII2ZOINSWNOGFThA",
    "FRANCE 24 English": "UCQfwfsi5VrQ8yKZ-UWmAEFg",
    "NBC News": "UCeY0bbntWzzVIaj2z3QigXg",
    "The New York Times": "UCqnbDFdCpuN8CMEg0VuEBqA",
    "Sky News": "UCoMdktPbSTixAyNGwb-UYkQ",
    "TIME": "UC8Su5vZCXWRag13H53zWVwA",
    "The Sun": "UCIzXayRP7-P0ANpq-nD-h5g",
    "The Telegraph": "UCPgLNge0xqQHWM5B5EFH9Cg",
    "USA TODAY": "UCP6HGa63sBC7-KHtkme-p-g",
    "Wall Street Journal": "UCK7tptUDHh-RYDsdxO1-5QQ",
    "Washington Post": "UCHd62-u_v4DvJ8TCFtpi4GA",
    "Bloomberg": "UCUMZ7gohGI9HcU9VNsr2FJQ",
    "RT America": "UCczrL-2b-gYK3l4yDld4XlQ",
    "Al Jazeera English": "UCNye-wNBqNL5ZzHSJj3l8Bg"
}

youtube = googleapiclient.discovery.build("youtube", "v3", developerKey="AIzaSyCM8Gde_A76UIuJT6tePlQ6azzZAfqC65k")

def getNewsVideos(identifier):
    videos = []

    try:
        for id in newsChannels.values():
            response = youtube.search().list(
                part="snippet",
                channelId=id,
                order="date",
                regionCode="US",
                relevanceLanguage="en",
                type="video"
            ).execute()

            videos.extend(list(map(lambda v: video2article(v, politicsLabel, mapNewsID(v), identifier), response["items"])))
    except HttpError:
        pass
    
    return videos

def getSportsVideos():
    videos = []
    try:
        response = youtube.videos().list(
            part="snippet",
            chart="mostPopular",
            regionCode="US",
            videoCategoryId="17"
        ).execute()

        videos = sportsMap(response["items"])

        while "nextPageToken" in response:
            nextPageToken = response["nextPageToken"]

            response = youtube.videos().list(
                part="snippet",
                chart="mostPopular",
                pageToken=nextPageToken,
                regionCode="US",
                videoCategoryId="17"
            ).execute()

            videos.extend(sportsMap(response["items"]))
    except HttpError:
        pass

    return videos

def getVideos(identifier):
    videos = getSportsVideos()
    videos.extend(getNewsVideos(identifier))
    return videos

def mapNewsID(video):
    return video["id"]["videoId"]

def sportsFilter(videos):
    return list(filter(lambda v: v["snippet"]["channelTitle"] in sportsChannels, videos))

def sportsMap(videos):
    return list(map(lambda v: video2article(v, "sport", v["id"], None), sportsFilter(videos)))

def video2article(video, cat, id, identifier):
    article = {
        'title': video["snippet"]["title"],
        'source': video["snippet"]["channelTitle"],
        'access_date': datetime.datetime.now(),
        'category': cat,
        'url': "https://www.youtube.com/watch?v=" + id,
        'top_image_url': video["snippet"]["thumbnails"]["high"]["url"],
        'embed': id
    }

    try:
        text = " ".join(list(map(lambda t: t["text"], YouTubeTranscriptApi.get_transcript(id))))

        if cat == politicsLabel:
            article['category'] = identifier.predict_article(text)

        return useBlob(article, text)
    except YouTubeTranscriptApi.CouldNotRetrieveTranscript:
        return article

# Add Blob fields to article object
def useBlob(article, text):
    biasdetect = TextBlob(text)
    article.update({
        "polarity": biasdetect.sentiment.polarity,
        "subjectivity": biasdetect.sentiment.subjectivity
    })
    return article
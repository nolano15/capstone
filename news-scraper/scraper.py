# Author: Neil Wilcoxson
# Defines the fetcher class for news scraping and runs it on the defined interval

# General Scraper Utilities
import random, json

# Article Retrieval
import newspaper
from newspaper import news_pool
import nltk

# Database access
import pymongo
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, BulkWriteError
import datetime
import time

# Machine learning
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException
from topicIdentification import Identifier
from youtube import getVideos, useBlob

# Config File Stuff
with open("scraper_config.json") as config_file:
    config = json.load(config_file)

ARTICLE_HISTORY_DAYS = 2

# A necessary dependency for the newspaper library
nltk.download('punkt')

# The Fetcher class will store persistent data about news sources
class Fetcher:

    def __init__(self, language_code, mongo_connection_string):
        # DB Connection Stuff
        self.mongo_client = MongoClient("mongodb://admin:TAOD@" + mongo_connection_string + ":27017")[config['dbName']]
        self.article_info_db = self.mongo_client['article_info']
        self.source_list_db = self.mongo_client['source_list']

        # Newspaper Stuff
        self.language_code = language_code
        self.news_sources = []
        self.get_news_sites()

        # Machine Learning Stuff
        self.identifier = Identifier()

    def get_news_sites(self):
        news_site_urls = list(map(lambda x: x['url'], self.source_list_db.find({}, {'_id': 0, 'url': 1})))
        for url in news_site_urls:
            self.add_site(url)

    def add_site(self, url):
        src = newspaper.build(url, self.language_code)
        if src not in self.news_sources:
            self.news_sources.append(src)

    def get_new_articles(self):
        print("Checking for newly added sources...")
        self.get_news_sites()

        print("Building news sources...")
        for source in self.news_sources:
            source.build()

        print("Deleting outdated articles...")
        self.article_info_db.delete_many(
            {"access_date": {"$lt": datetime.datetime.now() - datetime.timedelta(days=ARTICLE_HISTORY_DAYS)}})

        print("Downloading articles...")
        news_pool.set(self.news_sources, threads_per_source=2)
        news_pool.join()

        print("Analyzing articles...")
        articles = []
        for source in self.news_sources:
            for article in source.articles:
                try:
                    # gather information about the article
                    article.parse()
                    article.nlp()

                    language = None
                    try:
                        language = detect(article.text)
                    except LangDetectException:
                        print("Could not detect language of " + article.title)
                    if language == 'en':
                        category = self.identifier.predict_article(article.text)

                    if language == 'en':
                        category = self.identifier.predict_article(article.text)

                        # add the article to the database
                        article_info = {
                            'title': article.title,
                            'authors': article.authors,
                            'source': source.url,
                            'access_date': datetime.datetime.now(),
                            'category': category,
                            'summary': article.summary,
                            'url': article.url,
                            'top_image_url': article.top_image,
                            'language': language
                        }

                        articles.append(useBlob(article_info, article.text))

                except newspaper.article.ArticleException:
                    print("Cannot download " + article.url)

        if len(articles) > 0:
            print("Adding " + str(len(articles)) + " articles to database...")
            while True:
                try:
                    self.article_info_db.insert_many(articles, ordered=False)
                    break
                except ConnectionFailure:
                    retry_interval = int(config['dbRetryInterval'])
                    print("Cannot connect to the database, trying again in " + str(retry_interval) + " seconds...")
                    time.sleep(retry_interval)
                except BulkWriteError:
                    # at least one of the articles was not added, but some may have, this is ok
                    pass
        else:
            print("No new articles to add right now!")

    def get_videos(self):
        videos = getVideos(self.identifier)
        print("Adding " + str(len(videos)) + " videos...")
        if len(videos) > 0:
            try:
                self.article_info_db.insert_many(videos, ordered=False)
            except BulkWriteError:
                pass

fetcher = Fetcher("en", config['mongoConnectionString'])

# Wait at specified time to check for articles
while True:
    start = datetime.datetime.now()
    fetcher.get_new_articles()
    fetcher.get_videos()
    finish = datetime.datetime.now()
    duration = finish - start
    remaining_wait = int(config['refreshFrequency']) - duration.total_seconds()
    if remaining_wait > 0:
        print("Waiting for " + str(remaining_wait) + " seconds...")
        time.sleep(remaining_wait)

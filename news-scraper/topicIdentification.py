# Author: Wyatt Snyder
# Date Created: 9/16/2019
# Date Last Modified: 9/23/2019
#
# This file is responsible for identifiying the topic of an article so that it can be sorted to be displayed to the user
# data set from
# - D. Greene and P. Cunningham. "Practical Solutions to the Problem of Diagonal Dominance in Kernel Document
#                                   Clustering", Proc. ICML 2006

# Term Frequency Inverse Document Frequency How the documents dimensions are reduced
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


class Identifier:
    # initializes
    def __init__(self):
        model_exists = False

        # Read pickled file into model
        # if successful set model_exists to True

        # If no previous model exists create a new one
        if not model_exists:
            self.model, self.vectorizer, self.category_to_id, self.id_to_category = create_standard_model()

    def predict_article(self, article):
        article = "\"" + article.replace("\"", "\"\"") + "\""
        content = [article]
        article_features = self.vectorizer.transform(content).toarray()
        return self.id_to_category[self.model.predict(article_features)[0]]


def create_standard_model():
    model = LogisticRegression(random_state=0, solver='lbfgs', multi_class='auto')

    model, vectorizer, category_to_id, id_to_category = train_model_standard(model)

    return model, vectorizer, category_to_id, id_to_category


def train_model_standard(model):
    df = pd.read_csv(os.getcwd() + '/data.csv', encoding='latin1')
    category_id_df = df[['category', 'category_id']].drop_duplicates().sort_values('category_id')
    category_to_id = dict(category_id_df.values)
    id_to_category = dict(category_id_df[['category_id', 'category']].values)

    vectorizer = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2),
                                 stop_words='english')

    features = vectorizer.fit_transform(df.content).toarray()
    labels = df.category_id

    model.fit(features, labels)

    return model, vectorizer, category_to_id, id_to_category

def test_models():
    df = pd.read_csv(os.getcwd() + '/data.csv', encoding='latin1')
    category_id_df = df[['category', 'category_id']].drop_duplicates().sort_values('category_id')
    category_to_id = dict(category_id_df.values)
    id_to_category = dict(category_id_df[['category_id', 'category']].values)

    print(df.content)
    # Graph chart and display it for visualization of amount of files in each category

    plot = df.groupby('category').filename.count().plot.bar(ylim=0)
    plt.show()

    # get features from each article
    vectorizer = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2),
                                 stop_words='english')

    features = vectorizer.fit_transform(df.content).toarray()
    labels = df.category_id

    # plot a sample of the data on a scatter plot

    sample_size = int(len(features) * 0.3)
    np.random.seed(0)
    indices = np.random.choice(range(len(features)), size=sample_size, replace=False)
    projected_features = TSNE(n_components=2, random_state=0).fit_transform(features[indices])
    colors = ['pink', 'green', 'blue', 'orange', 'darkgrey']
    for category in category_to_id:
        category_id = category_to_id[category]
        points = projected_features[(labels[indices] == category_id).values]
        plt.scatter(points[:, 0], points[:, 1], s=30, c=colors[category_id], label=category)
    plt.title("tf-idf feature vector for each article, projected on 2 dimensions.")
    plt.legend()
    plt.show()

    # currently only one, can add more to test later
    models = [
        LogisticRegression(random_state=0, solver='lbfgs', multi_class='auto'),
        MultinomialNB(),
        MLPClassifier(solver='lbfgs'),
        RandomForestClassifier(n_estimators=10)
    ]

    # Cross validation test each model

    CV = 5
    cv_df = pd.DataFrame(index=range(CV * len(models)))
    entries = []
    for model in models:
        model_name = model.__class__.__name__
        accuracies = cross_val_score(model, features, labels, scoring="accuracy", cv=CV)
        for fold_idx, accuracy in enumerate(accuracies):
            entries.append((model_name, fold_idx, accuracy))
        cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])

    sns.boxplot(x='model_name', y='accuracy', data=cv_df)
    plt.show()
    print(cv_df.groupby("model_name").accuracy.mean())

    # Create confusion matrix for each model

    x_train, x_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index,
                                                                                     test_size=0.33, random_state=0)
    for model in models:
        model_name = model.__class__.__name__
        model.fit(x_train, y_train)
        y_pred_proba = model.predict_proba(x_test)
        y_pred = model.predict(x_test)

        conf_mat = confusion_matrix(y_test, y_pred)
        sns.heatmap(conf_mat, annot=True, fmt='d', xticklabels=category_id_df.category.values,
                    yticklabels=category_id_df.category.values)
        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.title(model_name + " confusion matrix")
        plt.show()

    model = models[0]

    # fit model to all the data
    model.fit(features, labels)

    # test on a single article
    dirc = os.getcwd()
    with open(os.path.join(dirc, "test_article.txt")) as curr_file:
        file_text = curr_file.readlines()
        content = "\"" + ((" ".join(file_text[2:])).replace("\"", "\"\"")) + "\""
        content = [content]
        file_features = vectorizer.transform(content).toarray()
        print(id_to_category[model.predict(file_features)[0]])


if __name__ == "__main__":
    ident = Identifier()
    dirc = os.getcwd()
    with open(os.path.join(dirc, "test_article.txt")) as curr_file:
        file_text = curr_file.readlines()
        article = " ".join(file_text)

        print(ident.predict_article(article))

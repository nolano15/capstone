'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  CLIENT_PORT: 80,
  BACKEND_ROOT_URL: '"https://capg1.com:5000/api/"'
})

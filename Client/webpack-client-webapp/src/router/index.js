import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/Dashboard'
import Profile from '../components/Profile'
import Topic from '../components/Topic'
import Search from '../components/Search'
import UserGuide from '../components/UserGuide'

Vue.use(VueRouter)
Vue.use(require('vue-cookie'))

Vue.mixin({
  methods: {
    xhrGetWithCred: endpoint => {
      let http = new XMLHttpRequest()
      http.open('GET', process.env.BACKEND_ROOT_URL + endpoint)
      http.withCredentials = true
      http.send()
      return http
    },
    xhrPutWithCred: endpoint => {
      const http = new XMLHttpRequest()
      http.open('PUT', process.env.BACKEND_ROOT_URL + endpoint, true)
      http.setRequestHeader('Content-type', 'application/json; charset=utf-8')
      http.withCredentials = true
      return http
    },
    adaptArticle: article => {
      return {
        id: article._id.$oid,
        image: article.top_image_url,
        title: article.title.replace(/&amp;/gm, '&').replace(/&#39;/gm, "'").replace(/&quot;/gm, '"'),
        summary: article.summary,
        subjectivity: Math.round(article.subjectivity * 100),
        polarity: Math.round(Math.abs(article.polarity) * 100),
        positive: article.polarity >= 0,
        url: article.url,
        vidId: article.hasOwnProperty('embed') ? article.embed : null
      }
    }
  }
})

export default new VueRouter({
  mode: 'history',
  props: ['mode'],
  routes: [
    {
      path: '/',
      redirect: { name: 'Dashboard' }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      props: this.mode
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      props: this.mode
    },
    {
      path: '/topics/:topicName',
      name: 'Topic',
      component: Topic,
      props: this.mode
    },
    {
      path: '/search',
      name: 'Search',
      component: Search,
      props: this.mode
    },
    {
      path: '/user_guide',
      name: 'UserGuide',
      component: UserGuide,
      props: this.mode
    }
  ]
})

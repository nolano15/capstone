/// Author: Neil Wilcoxson
/// Executes Mongo queries which configure the database from scratch

use admin
db.createUser({
    user: "admin",
    pwd: "TAOD",
    roles: [{role: "userAdminAnyDatabase", db: "admin"}, {role: "readWrite", db: "ambassador"}, {role: "dbAdmin", db: "ambassador"}],
    mechanisms: ["SCRAM-SHA-1"]
})

use ambassador

// Database creation
// Create an admin user
/*
db.createUser({
    user: "admin",
    pwd: "TAOD",
    roles: ["readWrite", "dbAdmin"],
    mechanisms: ["SCRAM-SHA-1"]
})
*/

// Collection creation
db.createCollection('article_info')
db.createCollection('source_list')
db.createCollection('users')
db.createCollection('videos')
db.createCollection('ratings')

// Unique constraints
db.getCollection('article_info').createIndex({url: 1}, {unique:true})
db.getCollection('source_list').createIndex({url: 1}, {unique:true})
db.getCollection('videos').createIndex({url: 1}, {unique:true})
db.getCollection('ratings').createIndex( { article_id: 1, user_id: 1 }, { unique: true } )

// Search constraints
db.getCollection('article_info').ensureIndex({title: "text", summary: "text"},{language_override: "dummy"})

// Default news sources
db.getCollection('source_list').insertMany([
    {url: 'http://foxnews.com'},
    {url: 'http://cnn.com'},
    {url: 'https://abcnews.go.com'},
    {url: 'https://www.cbsnews.com'},
    {url: 'https://www.cnbc.com'},
    {url: 'https://www.c-span.org'},
    {url: 'https://www.chicagotribune.com'},
    {url: 'https://www.nytimes.com'},
    {url: 'https://www.nbcnews.com'},
    {url: 'https://www.bbc.com/news'},
    {url: 'https://www.npr.org/sections/news'},
    {url: 'https://www.usatoday.com/news'}
])

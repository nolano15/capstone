# Author: Wyatt Snyder
# Date Created: 9/6/2019
# Date Last Modified: 9/8/2019
#
# This file is responsible for extracting topics from articles and sorting them into categories to be displayed
# to the user

import newspaper

# Takes a newspaper.build object and extracts the categories from it returning a dictionary mapping the categories to
# a new newspaper object containing each of the articles in said category, setting verbose to true will print various
# debugging messages
def sort_articles(paper, verbose=False):
    categories = {}

    if verbose:
        print("amount of articles given: ", paper.size())

    for category in paper.category_urls():
        if verbose:
            print("category: ", category)

        category_paper = newspaper.build(category)
        if category_paper.size() is not 0:
            categories.update([(category, category_paper)])
        elif verbose:
            print("found empty category")

        if verbose:
            print("category size: ", category_paper.size())
            print("list of articles in category: ", category_paper.articles)

    return categories

# For testing purposes only, file is not intended to be ran as main in primary application
def topicExtraction():
    # if you don't set memoize articles to false it will wipe all preexisting articles it found and only give new ones
    cnn_paper = newspaper.build('http://cnn.com', memoize_articles=False)

    categories = sort_articles(cnn_paper)

    topics = ""

    for key in categories:
        topics += addHTMLTags("articles in category: " + key, "div")
        for article in categories[key].articles:
            topics += addHTMLTags(article.url, "p")
    
    return topics

def addHTMLTags(string, tag):
    return "<" + tag + ">" + string + "</" + tag + ">"

    # url = 'http://fox13now.com/2013/12/30/new-year-new-laws-obamacare-pot-guns-and-drones/'
    # article = newspaper.Article(url)
    #
    # article.download()
    # article.parse()
    # for author in article.authors:
    #     print("author")
    #     print(author)
    #
    # article.nlp()
    #
    # print(article.keywords)
    # print(article.summary)
